#!/bin/sh

if [ -z "$SERVICE_URL" ]; then
  echo "SERVICE_URL is missing"
  exit 1
fi

USER=$(id -u)
GROUP=$(id -g)
SERVICE="$SERVICE_URL:$USER:$GROUP:/web:SHELL"

exec /usr/bin/shellinaboxd -n --disable-utmp-logging \
  --unixdomain-only "/var/run/mc-app-socket/socket:$USER:$GROUP:0600" --service $SERVICE --disable-ssl
