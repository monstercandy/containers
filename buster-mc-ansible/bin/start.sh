#!/bin/bash

set -e

ROOT_DIR=/mc
KEY_DIR="$ROOT_DIR/pki"
SSH_DIR="$ROOT_DIR/.ssh"
MC_DIR="$ROOT_DIR/ansi"

mkdir "$SSH_DIR" 2>/dev/null || true
mkdir "$KEY_DIR" 2>/dev/null || true
mkdir "$ROOT_DIR/inventory" 2>/dev/null || true
mkdir "$MC_DIR" 2>/dev/null || true

chmod go-rwx "$KEY_DIR"
if [ ! -s "$KEY_DIR/index.txt" ]; then
  touch "$KEY_DIR/index.txt"
fi
if [ ! -s "$KEY_DIR/serial" ]; then
  echo 01 >"$KEY_DIR/serial"
fi

if [ ! -e "$SSH_DIR/id_rsa.pub" ]; then 
  ssh-keygen
  echo "A new keypair was generated for SSH. You may distribute the following public key:"
  cat "$SSH_DIR/id_rsa.pub"
fi

cd $MC_DIR
if [ ! -d "monster-cloud" ]; then
  git clone https://monsterbb@bitbucket.org/monstercandy/mc-ansible.git . || true
fi

echo "Example execution:"
echo " ansible-playbook --verbose -i $ROOT_DIR/inventory/your-plant.yml $MC_DIR/monster-cloud/site.yml"
echo

exec ssh-agent /bin/bash -c "ssh-add $SSH_DIR/id_rsa && /bin/bash"

