#!/bin/bash

set -e

MAIN_DIR="$1"
shift

if [ -z "$2" ]; then
   echo "Usage: $0 cwd srcfile1 [srcfile2 ...] dst_directory"
   exit 1
fi


cd "$MAIN_DIR"

mv "$@"
