#!/bin/bash

# no set -e!

MAIN_DIR="$1"
shift

if [ -z "$MAIN_DIR" ]; then
   echo "Usage: $0 cwd"
   exit 1
fi


cd "$MAIN_DIR"


if [ ! -n "$(ls -A 2>/dev/null)" ]; then
 echo "Directory is empty"
 exit 0
fi

for f in *; do
 if findmnt $f 2>/dev/null; then
  echo "ERROR: $f is a mount point (findmnt)"
  exit 1
 fi

 if mountpoint $f; then
  echo "ERROR: $f is a mount point (mountpoint)"
  exit 2
 fi

    # also, we always return with success:
    rm --one-file-system -rfv $f
done

