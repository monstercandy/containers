#!/bin/bash

set -e

MAIN_DIR="$1"
shift

if [ -z "$MAIN_DIR" ]; then
   echo "Usage: $0 cwd"
   exit 1
fi


cd "$MAIN_DIR"

tar "czf" - --one-file-system $(find -maxdepth 1 -user $UID -printf '%P\n')
