#!/bin/bash

DSTDIR=$1
shift

if [ -z "$DSTDIR" ]; then
   echo "Usage: $0 dstdir"
   exit 1
fi

umask 077
mkdir -p "$DSTDIR/cur"
mkdir -p "$DSTDIR/new"
mkdir -p "$DSTDIR/tmp"
