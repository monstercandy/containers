#!/usr/bin/perl

use warnings;
use strict;
use Fcntl ':mode';

my $from_uid = shift @ARGV;
my $pm = shift @ARGV;
my $mainpath = shift @ARGV;
my $dry = shift @ARGV || 0;
die "Usage: $0 from_uid (p|m) path [dry]" if((!$mainpath)||(!-d $mainpath));


my $pmcmd = ($pm eq "p" ? "+i" : ($pm eq "m" ? "-i" : ""));
die "Invalid pm: $pm" if(!$pmcmd);
die "Invalid path" if($mainpath !~ m#^/.+#);

$mainpath =~ s#/+$##g;

my $readonly_file = "$mainpath/.readonly";

if($pm eq "p") {
   open(my $ki, ">$readonly_file");
   close($ki) if($ki) ;
}

dowork($mainpath, 1);

cmd("chattr", $pmcmd, $mainpath);

if($pm eq "m") {
   unlink($readonly_file);
}


sub dowork {
  my $path = shift;
  my $toplevel = shift;

  opendir(my $D, $path) or die "Could not opendir: $path / $!";
  my @ds = grep {!/^\.\.?$/} readdir($D);
  closedir($D);

  my $recursive_top = 1;
  my $single_chattr = 0;
again:
  for my $ad (@ds) {
     if(($toplevel)&&($ad =~ /^(dbbackup|tmp|sessions|php-fpm-logs)$/)) {
        $recursive_top = 0;
        print STDERR "Handling dir specially: $ad\n";
        next;
     }

     my $fullpath = "$path/$ad";
     my @ls = lstat($fullpath);
     my $mode = $ls[2];
     my $uid = $ls[4];

     # print STDERR "$fullpath: $mode/$uid\n";
     if($uid != $from_uid) {
        # print STDERR "different uid\n";
        $recursive_top = 0;
        next;
     }

     if((S_ISDIR($mode))||(S_ISREG($mode))) {
        if($single_chattr)  {
          dowork($fullpath) if(S_ISDIR($mode));
          cmd("chattr", $pmcmd, $fullpath);
        }

     } else {
        # print STDERR "special file\n";
        $recursive_top = 0;
     }
  }

  return if($single_chattr);

  if($recursive_top) {
     cmd("chattr", "-R", $pmcmd, $path); # recursively chattr'ing the top dir and then return
     return;
  }

  # print STDERR "Running new round for $path\n";
  $single_chattr = 1;
  goto again;
}

sub cmd {
  my @c = @_;
  my $cmd = ($dry ? "#": "").join(" ", @c)."\n";
  $cmd =~ s#\Q$mainpath\E#/#;
  print STDOUT $cmd;
  if(!$dry) {
     system(@c);
  }
}
