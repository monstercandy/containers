#!/bin/bash

set -e

MAIN_DIR="$1"
shift

if [ -z "$MAIN_DIR" ]; then
   echo "Usage: $0 find_root find_params..."
   exit 1
fi


cd "$MAIN_DIR"

find . "$@"
