#!/bin/bash

set -e

SRC_FILE_PATH="$1"

if [ -z "$SRC_FILE_PATH" ]; then
   echo "Usage: $0 src_file"
   exit 1
fi

cat "$SRC_FILE_PATH"
