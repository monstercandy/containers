#!/bin/bash

set -e

MAIN_DIR="$1"
shift

if [ -z "$MAIN_DIR" ]; then
   echo "Usage: template.sh destination_directory"
   echo "This will copy all files from /mnt."
   exit 1
fi


mkdir -p "$MAIN_DIR" || true
cd $MAIN_DIR
cp -prvn /mnt/* .
