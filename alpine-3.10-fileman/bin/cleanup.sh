#!/bin/bash

# no set -e!

MAIN_DIR="$1"
shift

if [ -z "$MAIN_DIR" ]; then
   echo "Usage: $0 cwd"
   exit 1
fi


cd "$MAIN_DIR"

# also, we always return with success:
rm --one-file-system -rfv $(find -maxdepth 1 -user $UID -printf '%P\n') || true
