#!/bin/bash

set -e

DST_FILE_PATH="$1"

if [ -z "$DST_FILE_PATH" ]; then
   echo "Usage: $0 dst_file"
   exit 1
fi

DIR=$(dirname "$DST_FILE_PATH")
mkdir -p "$DIR" 2>/dev/null || true

cat > "$DST_FILE_PATH"
