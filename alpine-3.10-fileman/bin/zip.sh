#!/bin/bash

set -e

MAIN_DIR="$1"
shift

ZIP_NAME="$1"
shift

if [ -z "$1" ]; then
   echo "Usage: $0 cwd name.zip files_to_zip"
   exit 1
fi


cd "$MAIN_DIR"

zip -y -r "$ZIP_NAME" "$@"
