#!/bin/bash

set -e

MAIN_DIR="$1"
shift
TAR_PARAMS="$1"
shift

if [ -z "$1" ]; then
   echo "Usage: $0 cwd [z] files_to_tgz"
   exit 1
fi


cd "$MAIN_DIR"

tar "c${TAR_PARAMS}f" - --one-file-system "$@"
