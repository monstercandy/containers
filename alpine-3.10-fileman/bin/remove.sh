#!/bin/bash

set -e

MAIN_DIR="$1"
shift

if [ -z "$1" ]; then
   echo "Usage: $0 cwd files_to_remove"
   exit 1
fi


cd "$MAIN_DIR"

rm --one-file-system -rfv "$@"
