#!/bin/bash

set -e

DST_DIR_PATH="$1"
TAR_PARAMS="$2"

if [ -z "$DST_DIR_PATH" ]; then
   echo "Usage: $0 dst_dir tar_params"
   exit 1
fi


cd "$DST_DIR_PATH"

tar "xv$TAR_PARAMS"
