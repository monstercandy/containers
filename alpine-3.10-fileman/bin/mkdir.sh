#!/bin/bash

set -e

DIR_TO_CREATE="$1"

if [ -z "$DIR_TO_CREATE" ]; then
   echo "Usage: $0 dir_to_create"
   exit 1
fi


mkdir -p "$DIR_TO_CREATE"
