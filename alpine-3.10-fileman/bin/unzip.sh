#!/bin/bash

set -e

MAIN_DIR="$1"
DST_DIR_PATH="$2"

if [ -z "$DST_DIR_PATH" ]; then
   echo "Usage: $0 main_dir dst_dir"
   exit 1
fi


cd "$MAIN_DIR$DST_DIR_PATH"

ZIPNAME="$MAIN_DIR/upload-$(date '+%s').zip"

cat > "$ZIPNAME"

unzip "$ZIPNAME"

rm "$ZIPNAME"
