#!/bin/bash

MAINDIR=$1
shift
PASSWORD=$1
shift

if [ -z "$PASSWORD" ]; then
   echo "Usage: $0 maindir password [other_tar_params ...]"
   exit 1
fi

cd "$MAINDIR"

tar "$@" -czf - * | openssl aes-128-cbc -k "$PASSWORD" -e 
