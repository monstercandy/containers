#!/bin/sh

IPCDIR=/web/ipc
REDISDIR=/web/redis
SYMLINK="$IPCDIR/redis"
CURRENT=$(readlink $SYMLINK)
THIS_SOCKET="/web/ipc/$CONTAINER_ID"

if [ "$CURRENT" != "$CONTAINER_ID" ]; then
  mkdir "$IPCDIR"
  rm $SYMLINK
  ln -s "$CONTAINER_ID" "$SYMLINK"
fi

# note: redis periodically flushes its data store to disk; by default to the current directory, so just leave this line here
mkdir "$REDISDIR"

exec redis-server --unixsocket "$THIS_SOCKET" --unixsocketperm 755 --maxmemory 50M --dir "$REDISDIR"
