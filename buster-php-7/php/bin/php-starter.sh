#!/bin/bash

mkdir /web/php-fpm-logs

SW_MAJOR=$(cat /usr/local/php/version.major)

export PHP_INI_SCAN_DIR="/web/conf/php-$PHP_MAJOR/"
mkdir -p "$PHP_INI_SCAN_DIR"

exec /usr/local/php/sbin/php-fpm
