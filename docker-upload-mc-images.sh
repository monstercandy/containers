#!/bin/bash

REPO=$1
if [ -z "$REPO" ]; then
   echo "Usage: $0 docker-repo"
   exit 1
fi

IMAGES=$(docker images --filter "label=com.monster-cloud.mc-app" | grep -v latest | grep -v REPOSITORY | awk '{print $1 ":" $2}' | grep -v stretch- | grep -v $REPO/ )
for IMAGE in $IMAGES; do
  echo "Image: $IMAGE"
  IIMAGE="$REPO/$IMAGE"
  docker tag "$IMAGE" "$IIMAGE"
  docker push "$IIMAGE"
done

