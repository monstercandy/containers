MonsterCandy Docker Images
==========================
This repo contains the Dockerfile's for the images used in the MonsterCandy shared webhosting admin panel.

Some helpful shell magic:

```
alias dattach="docker attach --sig-proxy=false"
alias drebuild='DOCKERIMAGE=$(basename $(pwd)); DOCKERTAG=$(cat Dockertag); docker build -t $DOCKERIMAGE:$DOCKERTAG .'
alias dtop='docker stats $(docker ps -q)'
alias dhup='docker kill --signal=HUP'
alias dlogs='docker logs'

function dsh() {
  docker exec -it $1 bash
}

function dbuildn() {
  export DOCKERCOUNTER=$(($DOCKERCOUNTER+1))
  DOCKERTAG=$(date +%Y-%m-%d)--$(printf "%03d" $DOCKERCOUNTER)
  DOCKERIMAGE=$1
  shift
  docker build -t $DOCKERIMAGE:$DOCKERTAG "$@" . && echo $DOCKERTAG > Dockertag
}

function dbuild() {
  dbuildn "$(basename $(pwd))" "$@"
}
```

How to use these images?
========================

The MonsterCandy system can be installed using the following Ansible tooling:
https://bitbucket.org/monstercandy/mc-ansible/src/master/

