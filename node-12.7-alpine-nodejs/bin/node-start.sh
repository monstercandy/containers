#!/bin/sh

set -e

cd "$1"
npm install
exec npm start
