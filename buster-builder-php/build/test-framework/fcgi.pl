#!/usr/bin/perl

use strict;
use warnings;
use FCGI::Client;
use IO::Socket::INET;

my $SCRIPT_NAME = shift @ARGV;
if(($SCRIPT_NAME)&&($SCRIPT_NAME =~ /^\d+$/)) {
  push @ARGV, $SCRIPT_NAME;
  $SCRIPT_NAME = "/php-fpm-status.php";
}
die "Usage: $0 [scriptname] port1 [port2 ...]" if(!scalar @ARGV);

for my $p (@ARGV) {
    my $sock = IO::Socket::INET->new(
        PeerAddr => '127.0.0.1',
        PeerPort => $p,
    ) or die $!;
    my $client = FCGI::Client::Connection->new( sock => $sock );
    my ( $stdout, $stderr, $appstatus ) = $client->request(
        +{
            REQUEST_METHOD => 'GET',
            SCRIPT_NAME   => $SCRIPT_NAME,
            SCRIPT_FILENAME   => $SCRIPT_NAME,
        },
        ''
    );

  die "No header part" if($stdout !~ /\r\n\r\n(.*)/s);

  print "$1\n";
  print "$stderr\n" if($stderr);
  print "$appstatus\n" if($appstatus);
}
