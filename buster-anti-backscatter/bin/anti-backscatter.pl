#!/usr/bin/perl

use warnings;
use strict;
use Carp qw(verbose);
use Sendmail::Milter qw(:all);

my $unix_sock = shift @ARGV || "/var/run/mc-app-socket/socket";
my $miltername = shift @ARGV || "anti-backscatter";

local $| = 1;

if (-e $unix_sock)
{
  print "Attempting to unlink UNIX socket '$unix_sock' ... ";

  if (unlink($unix_sock) == 0)
  {
    print "failed.\n";
    exit;
  }
  print "successful.\n";
}

my %cbs = (
  envfrom => sub {
     my $ctx = shift;
     if(scalar @_) {
        # NOTE: we are not doing this filter anymore as it seems to be not working as expected (probably because of the native milter library)

        # there was a MAIL FROM header, so this is not a Postfix DSN.
        # mydebug("Accepting mail, as it is not an internally generated one: @_");
        # return SMFIS_ACCEPT;
     }
     return SMFIS_CONTINUE;
  },
  header => sub {
     my ($ctx, $h, $v) = @_;
     if($h eq "From") {
      mydebug("$h: $v");
       # when it did not come from mailer daemon, accept it immediately
       return SMFIS_ACCEPT if($v !~ /^MAILER-DAEMON@/); 
     }
     return SMFIS_CONTINUE;
  },
  body => sub {
     my ($ctx, $body) = @_;
     if($body =~ /X-Spam-Flag: YES/) {
       mydebug("Discarding DSN as it is a backscatter spam");
       return SMFIS_DISCARD;
     } elsif($body =~ /X-Amavisd-Alert: INFECTED/) {
       mydebug("Discarding DSN as it is a backscatter virus");
       return SMFIS_DISCARD;
     } else {
      mydebug("Relaying simple DSN");
       return SMFIS_ACCEPT;
     }
  }
);


if(!Sendmail::Milter::setconn("local:$unix_sock"))
{
  print "Failed to detect connection information.\n";
  exit;
}

if(!Sendmail::Milter::register($miltername, \%cbs, SMFI_CURR_ACTS))
{
  print "Failed to register callbacks for $ARGV[0].\n";
  exit;
}

print "Starting Sendmail::Milter $Sendmail::Milter::VERSION engine.\n";

if (Sendmail::Milter::main())
{
  print "Successful exit from the Sendmail::Milter engine.\n";
}
else
{
  print "Unsuccessful exit from the Sendmail::Milter engine.\n";
}

sub mydebug {
  my $msg = shift;
  my $c = localtime;
  print "[$c] $msg\n";
}
