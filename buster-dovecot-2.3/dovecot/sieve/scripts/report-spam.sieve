require ["vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables"];

if environment :matches "imap.user" "*" {
  set "username" "";
}

pipe :copy "sa-learn.sh" [ "spam" ];
