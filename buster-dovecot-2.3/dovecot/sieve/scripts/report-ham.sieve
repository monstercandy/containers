require ["vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables"];

if environment :matches "imap.mailbox" "*" {
  set "mailbox" "";
}

if string "" "Trash" {
  stop;
}

if environment :matches "imap.user" "*" {
  set "username" "";
}

pipe :copy "sa-learn.sh" [ "ham" ];
