#!/bin/bash

if [ -z "$1" ]; then
   echo "Usage: $0 spam|ham"
   exit 1
fi

DIR=/storage/d1/mails/salearn-watchdir/
FN="$DIR/$$-$RANDOM-$1-$USER"

cat >>"$FN"
