#!/bin/sh

set -e

cd "$1"
export HOME="$1"
export PATH="$PATH:$HOME/.local/bin"
if [ -s requirements.txt ]; then
  pip install --no-cache-dir --user -r requirements.txt
fi

if [ ! -z "$2" ]; then
  cd "$2"
fi

exec uwsgi --socket /var/run/mc-app-socket/socket --plugins python3 --protocol uwsgi --wsgi "$3:application"
