#
# Server configuration
#
[server]

# Protocols to load
protocols=<<EOT
Postfix
Bizanga
EOT

# Modules to load
modules=<<EOT
Core
AccessControl
CheckHelo
CheckSPF
Greylisting
Quotas
EOT

# User to run this daemon as
# user=nobody
# group=nogroup

# Filename to store pid of parent process
pid_file=/dev/shm/cbpolicyd.pid

# Uncommenting the below option will prevent cbpolicyd going into the background
#background=no

# Preforking configuration
#
# min_server            - Minimum servers to keep around
# min_spare_servers     - Minimum spare servers to keep around ready to
#                         handle requests
# max_spare_servers     - Maximum spare servers to have around doing nothing
# max_servers           - Maximum servers alltogether
# max_requests          - Maximum number of requests each child will serve
#
# One may want to use the following as a rough guideline...
# Small mailserver:  2, 2, 4, 10, 1000
# Medium mailserver: 4, 4, 12, 25, 1000
# Large mailserver: 8, 8, 16, 64, 1000
#
min_servers=2
min_spare_servers=2
max_spare_servers=4
max_servers=10
max_requests=1000



# Log level:
# 0 - Errors only
# 1 - Warnings and errors
# 2 - Notices, warnings, errors
# 3 - Info, notices, warnings, errors
# 4 - Debugging
log_level=3

# File to log to instead of stdout
# log_file=/var/log/cbpolicyd.log

# note: log_file must be an empty string (otherwise it will default to some /var/log path).
log_file=

# NOTE: if you define log_mail, cbpolicyd will always (try to) send the log lines via syslog.
# Leave it undefined to go to stdout
#
# Log destination for mail logs...
# main          - Default. Log to policyd's main log mechanism, accepts NO args
# syslog        - log mail via syslog
#                       format: log_mail=facility@method,args
#
# Valid methods for syslog:
# native        - Let Sys::Syslog decide
# unix          - Unix socket
# udp           - UDP socket
# stream        - Stream (for Solaris)
#
# Example: unix native
#log_mail=mail@syslog:native
#
# Example: unix socket
#log_mail=mail@syslog:unix
#
# Example: udp
#log_mail=mail@syslog:udp,127.0.0.1
#
# Example: Solaris
#log_mail=local0@syslog:stream,/dev/log
#log_mail=maillog

# Things to log in extreme detail
# modules       - Log detailed module running information
# tracking      - Log detailed tracking information
# policies      - Log policy resolution
# protocols     - Log general protocol info, but detailed
# bizanga       - Log the bizanga protocol
#
# There is no default for this configuration option. Options can be
# separated by commas. ie. protocols,modules
#
#log_detail=

# IP to listen on, * for all
#host=*
#127.0.0.1

# Port to run on
# note: cluebringer does not dispatch the proto attribute from this config file.
# the /unix suffix will be parsed and understood by Net::Server:Proto,
# the app will be listening on /var/run/mc-app-socket/socket, as expected
port=/var/run/mc-app-socket/socket/unix

# Timeout in communication with clients
#timeout=120

# cidr_allow/cidr_deny
# Comma, whitespace or semi-colon separated. Contains a CIDR block to
# compare the clients IP to.  If cidr_allow or cidr_deny options are
# given, the incoming client must match a cidr_allow and not match a
# cidr_deny or the client connection will be closed.
#cidr_allow=0.0.0.0/0
#cidr_deny=



[database]
DSN=DBI:SQLite:dbname=/var/lib/monster/email/db-cluebringer/cluebringer.sqlite

#

# What do we do when we have a database connection problem
# tempfail      - Return temporary failure
# pass          - Return success
bypass_mode=tempfail

# How many seconds before we retry a DB connection
bypass_timeout=30



# Access Control module
[AccessControl]
enable=0

# Greylisting module
[Greylisting]
enable=1

# CheckHelo module
[CheckHelo]
enable=0

# CheckSPF module
[CheckSPF]
enable=0

# Quotas module
[Quotas]
enable=1

